package dlesiak.interview.game.dice;

class PenaltyPointsCalculator {
    private final Points maxPenaltyPoints;

    PenaltyPointsCalculator(Points maxPenaltyPoints) {
        this.maxPenaltyPoints = maxPenaltyPoints;
    }

    //TODO should input=player? or turn or primitives
    Points maxPenaltyPoints(Player player) {
        Points penaltyPoints = Points.zero();

        int totalThrows = player.getCurrentTurn().getTotalThrows();

        for (int throwNumber = 1; throwNumber <= totalThrows; throwNumber++) {
            penaltyPoints = penaltyPoints.add(maxPenaltyPoints.divide(throwNumber));
        }

        return penaltyPoints;
    }

    //TODO should input=player? or turn or primitives
    Points penaltyPoints(Player player, int throwResult) {
        int penaltyPoints = throwResult / player.getCurrentTurn().getCurrentThrow();
        return Points.of(penaltyPoints);
    }
}
