package dlesiak.interview.game.dice;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static dlesiak.interview.game.dice.DiceGameEngine.Status.FINISHED;
import static dlesiak.interview.game.dice.DiceGameEngine.Status.RUNNING;


final class DiceGameEngine {

    private final Predicate<Integer> firstThrowWinRule;
    private final Predicate<Integer> winRule;
    private final Predicate<Integer> firstThrowLoseRule;
    private final DiceThrower diceThrower;
    private final PenaltyPointsCalculator penaltyPointsCalculator;
    private final PlayerList players;

    DiceGameEngine(Player[] players, DiceThrower diceThrower, PenaltyPointsCalculator penaltyPointsCalculator, Predicate<Integer> firstThrowWinRule, Predicate<Integer> winRule, Predicate<Integer> firstThrowLoseRule) {
        Objects.requireNonNull(firstThrowWinRule);
        Objects.requireNonNull(firstThrowLoseRule);
        Objects.requireNonNull(diceThrower);

        this.players = new PlayerList(players);
        this.diceThrower = diceThrower;
        this.firstThrowWinRule = firstThrowWinRule;
        this.winRule = winRule;
        this.firstThrowLoseRule = firstThrowLoseRule;
        this.penaltyPointsCalculator = penaltyPointsCalculator;
    }

    Status nextStep() {
        if (anyPlayerCanContinueToPlay()) { //todo test
            nextTurn();
            return RUNNING;
        } else {
            return FINISHED;
        }
    }

    private void nextTurn() {
        while (currentPlayer().currentTurnFinished()) {
            players.switchPlayer();
        }

        int throwResult = diceThrower.throwDices();
        currentPlayer().finishThrow();

        if (firstThrowInTurn() && firstThrowWinRule.test(throwResult)) {
            nextPlayer();
        } else if (firstThrowInTurn() && firstThrowLoseRule.test(throwResult)) {
            Points penaltyPoints = penaltyPointsCalculator.maxPenaltyPoints(currentPlayer());
            currentPlayer().addPenaltyPoints(penaltyPoints);
            nextPlayer();
        } else if (winRule.test(throwResult)) {
            nextPlayer();
        } else if (throwResultIsDifferentThanRules(throwResult)) {
            Points penaltyPoints = penaltyPointsCalculator.penaltyPoints(currentPlayer(), throwResult);
            currentPlayer().addPenaltyPoints(penaltyPoints);
        }
    }

    private boolean throwResultIsDifferentThanRules(int throwResult) {
        return !(firstThrowLoseRule.test(throwResult) || firstThrowWinRule.test(throwResult) || winRule.test(throwResult));
    }

    private void nextPlayer() {
        currentPlayer().nextTurn();
        players.switchPlayer();
    }

    private boolean firstThrowInTurn() {
        return currentPlayer().isFirstThrowInTurn();
    }

    private boolean anyPlayerCanContinueToPlay() {
        return players
                .getPlayers()
                .stream()
                .anyMatch(Player::canPlay);
    }


    List<Player> getPlayers() {
        return players.getPlayers();
    }

    Player currentPlayer() {
        return players.getCurrentPlayer();
    }

    enum Status {
        FINISHED, RUNNING
    }
}
