package dlesiak.interview.game.dice;

public final class Turn {
    private final int initThrowNumber;
    private int numberOfThrow;

    public Turn(int numberOfThrows) {
        if (numberOfThrows < 1) {
            throw new IllegalArgumentException();
        }
        this.initThrowNumber = numberOfThrows;
        this.numberOfThrow = 0;
    }

    int getCurrentThrow() {
        return numberOfThrow;
    }

    boolean hasAnotherThrow() {
        return numberOfThrow < initThrowNumber;
    }

    void finishThrow() {
        if (!hasAnotherThrow()) {
            throw new IllegalStateException("No throws left");
        }
        this.numberOfThrow++;
    }

    boolean firstThrow() {
        return numberOfThrow == 1;
    }

    int getTotalThrows() {
        return initThrowNumber;
    }
}
