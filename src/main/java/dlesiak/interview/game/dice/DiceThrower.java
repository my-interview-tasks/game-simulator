package dlesiak.interview.game.dice;

public interface DiceThrower {
    //todo return DiceCollection object
    int throwDices();

    //todo return dices
    int diceTotalSides();
}
