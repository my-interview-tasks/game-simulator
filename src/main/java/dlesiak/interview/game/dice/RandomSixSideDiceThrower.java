package dlesiak.interview.game.dice;

import java.util.Random;

final class RandomSixSideDiceThrower implements DiceThrower {
    private final static int MAX_NUMBER = 6;
    private final static int MIN_NUMBER = 1;
    private final Random random = new Random();
    private final int diceNumber;

    RandomSixSideDiceThrower(int diceNumber) {
        this.diceNumber = diceNumber;
    }

    // return DiceObject not raw type?
    @Override
    public int throwDices() {
        int total = 0;
        for (int i = 0; i < diceNumber; i++)
            total += random.nextInt(MAX_NUMBER - MIN_NUMBER + 1) + MIN_NUMBER;

        return total;
    }

    @Override
    public int diceTotalSides() {
        return MAX_NUMBER * diceNumber;
    }
}
