package dlesiak.interview.game.dice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

final class PlayerList {
    private final Player[] players;
    private int currentPlayerIndex = 0;

    PlayerList(Player[] players) {
        Objects.requireNonNull(players);
        requireNonNullInArray(players);
        if (players.length < 1) {
            throw new IllegalArgumentException("At least one players required!");
        }
        validateUniquePlayers(players);
        this.players = players;
    }

    private void requireNonNullInArray(Player[] players) {
        for (Player player : players) {
            if (player == null) {
                throw new NullPointerException("Player cannot be null");
            }
        }
    }

    private void validateUniquePlayers(Player[] players) {
        if (new HashSet<>(Arrays.asList(players)).size() != players.length)
            throw new IllegalArgumentException("No unique players!");
    }

    void switchPlayer() {
        if (currentPlayerIndex < players.length - 1)
            currentPlayerIndex++;
        else
            currentPlayerIndex = 0;
    }

    List<Player> getPlayers() {
        return Arrays.asList(players);
    }

    Player getCurrentPlayer() {
        return players[currentPlayerIndex];
    }
}
