package dlesiak.interview.game.dice;

import java.util.Objects;

public final class Points implements Comparable<Points> {
    private final int value;

    private Points(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("Points cannot be negative");
        }
        this.value = value;
    }

    static Points of(int value) {
        return new Points(value);
    }

    static Points zero() {
        return new Points(0);
    }

    Points add(Points points) {
        return new Points(value + points.value);
    }

    Points divide(int divider) {
        if (divider <= 0) {
            throw new IllegalArgumentException("Divider has to be > 0");
        }
        return new Points(value / divider);
    }

    Points times(int multiplier) {
        if (multiplier < 0) {
            throw new IllegalArgumentException("Multiplier has to be > 0. Points cannot be negative!");
        }
        return Points.of(this.value * multiplier);
    }

    @Override
    public int compareTo(Points points) {
        return Integer.compare(value, points.value);
    }

    @Override
    public String toString() {
        return value + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Points points = (Points) o;
        return value == points.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
