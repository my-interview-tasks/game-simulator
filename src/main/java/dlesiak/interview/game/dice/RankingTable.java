package dlesiak.interview.game.dice;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public final class RankingTable {
    private final List<Player> ranking;

    RankingTable(List<Player> ranking) {
        Objects.requireNonNull(ranking);
        if (ranking.size() < 1) {
            throw new IllegalArgumentException("Ranking cannot be empty");
        }
        ranking.sort(Comparator.comparing(Player::getPoints));
        this.ranking = ranking;
    }

    public List<Player> getRanking() {
        return Collections.unmodifiableList(ranking);
    }

    public boolean isDraw() {
        if (ranking.size() <= 1) return false;
        Points first = ranking.get(0).getPoints();
        Points second = ranking.get(1).getPoints();
        return first.equals(second);
    }

    //TODO how to treat draw? getWinner? getWinners?
    public Player getWinner() {
        return ranking.get(0);
    }

    public List<Player> getWinners() {
        Player firstPlacePlayer = ranking.get(0);

        return ranking.stream()
                .filter(player -> firstPlacePlayer.getPoints().equals(player.getPoints()))
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }
}
