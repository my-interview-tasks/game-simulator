package dlesiak.interview.game.dice;

import java.util.List;

public final class DiceGameSimulator {

    private static final DiceGameEngineFactory diceGameEngineFactory = new DiceGameEngineFactory();
    private final DiceGameEngine diceGameEngine;

    private DiceGameSimulator(DiceGameEngine defaultGame) {
        this.diceGameEngine = defaultGame;
    }

    public static DiceGameSimulator defaultGameSimulator(Player[] players) {
        return new DiceGameSimulator(diceGameEngineFactory.defaultEngine(players));
    }

    public DiceGameEngine.Status run() {
        DiceGameEngine.Status status;
        do {
            status = diceGameEngine.nextStep();
        } while (DiceGameEngine.Status.RUNNING == status);
        return status;
    }

    public RankingTable getRanking() {
        List<Player> players = diceGameEngine.getPlayers();
        return new RankingTable(players);
    }

}
