package dlesiak.interview.game.dice;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

class DiceGameEngineFactory {
    private final static int DEFAULT_NUMBER_OF_DICE = 2;
    private final static Predicate<Integer> FIRST_THROW_WIN_RULE = diceThrowResult -> diceThrowResult == 7 || diceThrowResult == 11;
    private final static Predicate<Integer> WIN_RULE = diceThrowResult -> diceThrowResult == 5;
    private final static Predicate<Integer> FIRST_THROW_LOSE_RULE = diceThrowResult -> diceThrowResult == 2 || diceThrowResult == 12;
    private static List<Predicate<Integer>> rules = new ArrayList<>();
    private final Predicate<Integer> isRulesContainsNumber = rules
            .stream()
            .reduce(Predicate::or)
            .orElseThrow(() -> new IllegalArgumentException("No rules found"));


    static {
        rules.add(FIRST_THROW_LOSE_RULE);
        rules.add(WIN_RULE);
        rules.add(FIRST_THROW_WIN_RULE);
    }

    DiceGameEngine defaultEngine(Player[] players) {
        DiceThrower diceThrower = new RandomSixSideDiceThrower(DEFAULT_NUMBER_OF_DICE);
        Points maxPenaltyPoints = mostPessimisticResultOfTurn(diceThrower, rules);
        PenaltyPointsCalculator penaltyPointsCalculator = new PenaltyPointsCalculator(maxPenaltyPoints);
        return new DiceGameEngine(players, diceThrower, penaltyPointsCalculator, FIRST_THROW_WIN_RULE, WIN_RULE, FIRST_THROW_LOSE_RULE);
    }

    private Points mostPessimisticResultOfTurn(DiceThrower diceThrower, List<Predicate<Integer>> rules) {
        int max = diceThrower.diceTotalSides();
        totalSidesMoreThanZero(max);

        while (isRulesContainsNumber.test(max)) {
            if (--max < 0)
                throw new IllegalArgumentException("Invalid rules or dice config!");
        }
        return Points.of(max);
    }

    private void totalSidesMoreThanZero(int max) {
        if (max == 0)
            throw new IllegalArgumentException("Total sides has to be > 0");
    }

}
