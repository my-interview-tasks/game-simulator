package dlesiak.interview.game.dice;

import java.util.Objects;
import java.util.function.Supplier;

public final class Player {
    private final String name;
    private final Supplier<Turn> turnCreator;
    private Points penaltyPoints = Points.zero();
    private Turn currentTurn;
    private int turnLeft;

    public Player(String name, int turnQuantity, Supplier<Turn> turnCreator) {
        if (turnQuantity < 1) {
            throw new IllegalArgumentException("quantity has to >= 1");
        }
        this.name = name;
        this.turnCreator = turnCreator;
        this.turnLeft = turnQuantity;
        this.currentTurn = this.turnCreator.get();
        Objects.requireNonNull(currentTurn);
        turnLeft--;
    }

    void addPenaltyPoints(Points points) {
        this.penaltyPoints = this.penaltyPoints.add(points);
    }

    Turn getCurrentTurn() {
        if (currentTurn == null) {
            throw new IllegalStateException("Player has no turns left");
        }
        return currentTurn;
    }

    boolean canPlay() {
        return currentTurn != null && currentTurn.hasAnotherThrow();
    }

    boolean currentTurnFinished() {
        return currentTurn == null || !currentTurn.hasAnotherThrow();
    }

    void finishThrow() {
        this.currentTurn.finishThrow();
    }

    void nextTurn() {
        if (turnLeft > 0) {
            this.currentTurn = turnCreator.get();
            this.turnLeft--;
        } else {
            this.currentTurn = null;
        }
    }


    boolean isFirstThrowInTurn() {
        return currentTurn.firstThrow();
    }

    public String getName() {
        return name;
    }

    public Points getPoints() {
        return penaltyPoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


}
