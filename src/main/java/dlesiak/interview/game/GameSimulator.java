package dlesiak.interview.game;


import dlesiak.interview.game.dice.DiceGameSimulator;
import dlesiak.interview.game.dice.Player;
import dlesiak.interview.game.dice.RankingTable;
import dlesiak.interview.game.dice.Turn;

public class GameSimulator {

    private final static int NUMBER_OF_TURNS = 5;
    private final static int NUMBER_OF_THROWS_IN_TURN = 10;

    public static void main(String[] a) {

        // Points are integers so when dividing, points in turn can be 0.

        Player[] players = new Player[2];
        players[0] = new Player("pierwszy", NUMBER_OF_TURNS, () -> new Turn(NUMBER_OF_THROWS_IN_TURN));
        players[1] = new Player("drugi", NUMBER_OF_TURNS, () -> new Turn(NUMBER_OF_THROWS_IN_TURN));

        DiceGameSimulator diceGame = DiceGameSimulator.defaultGameSimulator(players);
        diceGame.run();
        printResult(diceGame);
    }


    private static void printResult(DiceGameSimulator diceGameSimulator) {
        RankingTable rankingTable = diceGameSimulator.getRanking();
        StringBuilder sb = new StringBuilder();
        if (rankingTable.isDraw()) {
            sb.append("Draw");
        } else {
            sb.append("No draw");
        }
        sb.append("\n");
        createRankingList(rankingTable, sb);
        System.out.println(sb.toString());
    }

    private static void createRankingList(RankingTable rankingTable, StringBuilder sb) {
        for (Player player : rankingTable.getRanking()) {
            sb.append("Player ")
                    .append(player.getName())
                    .append(" with ")
                    .append(player.getPoints())
                    .append(" points\n");
        }
    }
}
