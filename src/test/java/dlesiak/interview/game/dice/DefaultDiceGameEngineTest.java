package dlesiak.interview.game.dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultDiceGameEngineTest {

    private static final Points MAX_PENALTY_POINTS = Points.of(10);
    private final static Predicate<Integer> FIRST_THROW_WIN_RULE = diceThrowResult -> diceThrowResult == 7 || diceThrowResult == 11;
    private final static Predicate<Integer> WIN_RULE = diceThrowResult -> diceThrowResult == 5;
    private final static Predicate<Integer> FIRST_THROW_LOSE_RULE = diceThrowResult -> diceThrowResult == 2 || diceThrowResult == 12;
    private final static int SIX_SIDE_DICE = 6;
    private final static int NUMBER_OF_TURN = 5;
    private final static int MAX_TURN_THROWS = 10;

    private final Player playerOne = player("one");
    private final Player playerTwo = player("two");

    private final DiceThrower diceThrower = mock(DiceThrower.class);
    private DiceGameEngine diceGameEngine;

    @BeforeEach
    void createDefaultDiceGameEngine() {
        when(diceThrower.diceTotalSides()).thenReturn(SIX_SIDE_DICE);

        diceGameEngine = new DiceGameEngine(
                new Player[]{playerOne, playerTwo},
                diceThrower,
                new PenaltyPointsCalculator(MAX_PENALTY_POINTS),
                FIRST_THROW_WIN_RULE,
                WIN_RULE,
                FIRST_THROW_LOSE_RULE
        );
    }

    @Test
    void continueGameIfAnyPlayerCanStillPlay() {

    }

    @Test
    void cannotContinueGameIfAnyPlayerCanStillPlay() {

    }

    @Test
    void nextPlayerTurnIfCurrentPlayerHasNoThrowsLeft() {
        //given
        Player one = playerWithNoTurnLeft();
        diceGameEngine = new DiceGameEngine(
                new Player[]{one, playerTwo},
                diceThrower,
                new PenaltyPointsCalculator(MAX_PENALTY_POINTS),
                FIRST_THROW_WIN_RULE,
                WIN_RULE,
                FIRST_THROW_LOSE_RULE
        );

        //when
        one.nextTurn();
        diceGameEngine.nextStep();

        //then
        isAnotherPlayerTurn();
    }

    @Test
    void nextPlayerTurnIfCurrentPlayerHasEmptyTurn() {
        //given
        Player one = new Player("one", 1, () -> new Turn(1));
        diceGameEngine = new DiceGameEngine(
                new Player[]{one, playerTwo},
                diceThrower,
                new PenaltyPointsCalculator(MAX_PENALTY_POINTS),
                FIRST_THROW_WIN_RULE,
                WIN_RULE,
                FIRST_THROW_LOSE_RULE
        );

        //when
        when(diceThrower.throwDices()).thenReturn(9);
        one.nextTurn();
        diceGameEngine.nextStep();

        //then
        isAnotherPlayerTurn();
    }

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {7, 11})
    void playerWinTurnBeforeTimeIfFirstDiceThrowEqualsTo(int dice) {
        //given
        //win dice throw
        diceThrow(dice);

        //when
        diceGameEngine.nextStep();

        //then
        currentPlayerFinishTurn();
        isAnotherPlayerTurn();
    }

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {7, 11})
    void playerNotWinTurnBeforeTimeIfItIsNotFirstDiceThrowInTurn(int dice) {
        //given
        //player in middle of throws
        playerOne.finishThrow();
        playerOne.finishThrow();

        //when
        //playerOne win dice throw
        diceThrow(dice);
        diceGameEngine.nextStep();

        //then
        currentPlayerContinueTurn();
    }

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {2, 12})
    void playerLoseTurnBeforeTimeIfItIsNotFirstDiceThrowInTurnEqualsTo(int dice) {
        //given
        //lose dice throw
        diceThrow(dice);

        //when
        diceGameEngine.nextStep();

        //then
        currentPlayerGetMaxPenaltyPoints();
        currentPlayerFinishTurn();
        isAnotherPlayerTurn();
    }

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {2, 12})
    void playerNotLoseTurnBeforeTimeIfFirstDiceThrowEqualsTo(int dice) {
        //given
        //player in middle of throws
        playerOne.finishThrow();
        playerOne.finishThrow();

        //when
        diceThrow(dice);
        diceGameEngine.nextStep();

        //then
        currentPlayerContinueTurn();
    }

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {5})
    void playerWinTurnBeforeTimeIfDiceThrowEqualTo(int dice) {
        //given
        //win dice throw
        diceThrow(dice);

        //when
        diceGameEngine.nextStep();

        //then
        currentPlayerFinishTurn();
        isAnotherPlayerTurn();
    }

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {1, 3, 4, 6, 8, 9, 10})
    void playerGetsAdditionalPenaltyPointsIfDiceThrowEqualsTo(int dice) {
        //given
        diceThrow(dice);

        //when
        diceGameEngine.nextStep();

        //then
        currentPlayerGetPenaltyPoints();
        currentPlayerContinueTurn();
    }

    private void currentPlayerFinishTurn() {
        assertEquals(0, playerOne.getCurrentTurn().getCurrentThrow());
    }


    private void currentPlayerGetMaxPenaltyPoints() {
        assertEquals(1, playerOne.getPoints().compareTo(Points.zero()));
        assertEquals(0, playerOne.getCurrentTurn().getCurrentThrow());
    }

    private void isAnotherPlayerTurn() {
        assertEquals(playerTwo, diceGameEngine.currentPlayer());
    }

    private void currentPlayerContinueTurn() {
        assertEquals(playerOne, diceGameEngine.currentPlayer());
    }

    private void currentPlayerGetPenaltyPoints() {
        assertEquals(1, playerOne.getPoints().compareTo(Points.zero()));
        // assertEquals(0, playerOne.getCurrentTurn().getCurrentThrow());
    }

    private Player player(String one) {
        return new Player(one, NUMBER_OF_TURN, () -> new Turn(MAX_TURN_THROWS));
    }


    private void diceThrow(int dice) {
        when(diceThrower.throwDices()).thenReturn(dice);
    }

    private Player playerWithNoTurnLeft() {
        return new Player("one", 1, () -> new Turn(1));
    }
}