package dlesiak.interview.game.dice;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PenaltyPointsCalculatorTest {

    private static final Points MAX_POINTS = Points.of(100);
    private final PenaltyPointsCalculator calculator = new PenaltyPointsCalculator(MAX_POINTS);

    @ParameterizedTest
    @CsvSource({
            "5, 228",
            "2, 150",
            "1, 100"
    })
    void calculateMaxPenaltyPoints(int numberOfThrows, int result) {
        Player one = new Player("one", 2, () -> new Turn(numberOfThrows));

        Points points = calculator.maxPenaltyPoints(one);

        assertEquals(Points.of(result), points);
    }

    @ParameterizedTest
    @CsvSource({
            "3, 6, 2",
            "4, 56, 14",
            "8, 6, 0",
            "3, 3, 1"
    })
    void calculateAdditionalPenaltyPoints(int numOfThrows, int throwTotal, int result) {
        Player one = new Player("one", 2, () -> new Turn(10));
        throwDice(one, numOfThrows);

        Points points = calculator.penaltyPoints(one, throwTotal);

        assertEquals(Points.of(result), points);
    }

    private void throwDice(Player one, int numberOfThrows) {
        for (int i = 0; i < numberOfThrows; i++) {
            one.getCurrentTurn().finishThrow();
        }
    }


}