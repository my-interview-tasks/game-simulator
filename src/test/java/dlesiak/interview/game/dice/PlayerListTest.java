package dlesiak.interview.game.dice;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PlayerListTest {
    private final Player one = new Player("one", 1, () -> new Turn(1));
    private final Player two = new Player("two", 1, () -> new Turn(1));
    private final Player three = new Player("three", 1, () -> new Turn(1));
    private final Player four = new Player("four", 1, () -> new Turn(1));

    private final PlayerList playerList = new PlayerList(new Player[]{one, two, three, four});

    @Test
    @Order(1)
    void currentPlayerIsPlayerOne() {
        //expect
        assertEquals(one, playerList.getCurrentPlayer());
    }

    @Test
    @Order(2)
    void switchToSecondPlayer() {
        //when
        playerList.switchPlayer();

        //expect
        assertEquals(two, playerList.getCurrentPlayer());
    }

    @Test
    @Order(3)
    void switchToThirdPlayer() {
        //when
        playerList.switchPlayer();

        //expect
        assertEquals(three, playerList.getCurrentPlayer());
    }

    @Test
    @Order(4)
    void switchToFourthPlayer() {
        //when
        playerList.switchPlayer();

        //expect
        assertEquals(four, playerList.getCurrentPlayer());
    }

    @Test
    @Order(5)
    void switchToFirstPlayer() {
        //when
        playerList.switchPlayer();

        //expect
        assertEquals(one, playerList.getCurrentPlayer());
    }

}