package dlesiak.interview.game.dice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PointsTest {

    //TODO equality tests

    @Test
    void cannotCreateNegativePoints() {
        assertThrows(IllegalArgumentException.class, () -> Points.of(-4));
    }

    @Test
    void add() {
        Points one = Points.of(13);
        Points two = Points.of(2);
        Points sum = one.add(two);
        assertEquals(Points.of(15), sum);
    }

    @Test
    void divide() {
        Points one = Points.of(13);
        Points result = one.divide(2);
        assertEquals(Points.of(6), result);
    }

    @Test
    void cannotDivideByZero() {
        Points one = Points.of(13);
        assertThrows(IllegalArgumentException.class, () -> one.divide(0));
    }

    @Test
    void multiplying() {
        Points one = Points.of(13);
        Points result = one.times(2);
        assertEquals(Points.of(26), result);
    }

    @Test
    void cannotMultiplyByLessThanZero() {
        Points one = Points.of(13);
        assertThrows(IllegalArgumentException.class, () -> one.times(-1));
    }

    @Test
    void compareWhenDifferent() {
        Points one = Points.of(13);
        Points two = Points.of(2);

        assertEquals(1, one.compareTo(two));
        assertEquals(-1, two.compareTo(one));
    }

    @Test
    void compareWhenEqual() {
        Points one = Points.of(13);
        Points two = Points.of(13);

        assertEquals(0, one.compareTo(two));
        assertEquals(0, two.compareTo(one));
    }


}