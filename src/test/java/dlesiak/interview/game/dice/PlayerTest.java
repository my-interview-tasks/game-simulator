package dlesiak.interview.game.dice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    //TODO test equality

    @ParameterizedTest(name = "turns = {index}")
    @ValueSource(ints = {0, -2, -10})
    void throwErrorIfNumberOfTurnsIsLessThanOrEqualToZero(int turnQuantity) {
        assertThrows(IllegalArgumentException.class
                , () -> new Player("one", turnQuantity, () -> new Turn(0)));
    }

    @Test
    void throwErrorIfTurnSupplierReturnsNull() {
        assertThrows(NullPointerException.class
                , () -> new Player("one", 2, () -> null));
    }

    @Test
    void canNotPlayIfNoTurnLeft() {
        //given
        //player with 2 x turn
        Player one = new Player("one", 2, () -> new Turn(1));
        //and
        //all turns finished
        one.nextTurn();
        one.nextTurn();

        //expect
        assertFalse(one.canPlay());
    }

    @Test
    void canNotPlayIfLastTurnHasNoThrows() {
        //given
        //player with 2 x turn
        Player one = new Player("one", 2, () -> new Turn(1));
        //and
        //first turn finished
        one.nextTurn();
        //second turn has no throws
        one.finishThrow();

        //expect
        assertFalse(one.canPlay());
    }

    @Test
    void canPlayIfHasTurnLeft() {
        //given
        //player with 2 x turn
        Player one = new Player("one", 2, () -> new Turn(1));
        //and
        //one turn finished
        one.nextTurn();

        //expect
        assertTrue(one.canPlay());
    }

    @Test
    void turnFinishedIfHasNoAnotherThrows() {
        //given
        //player with 2 x turn
        Player one = new Player("one", 2, () -> new Turn(1));
        //and
        //turn has no throws left
        one.finishThrow();

        //expect
        assertTrue(one.currentTurnFinished());
    }

    @Test
    void turnNotFinishedIfHasThrowsLeft() {
        //given
        //player with 2 x turn
        Player one = new Player("one", 2, () -> new Turn(1));

        //expect
        assertFalse(one.currentTurnFinished());
    }

    @Test
    void throwErrorIfNoCurrentTurn() {
        //given
        //player with 2 x turn
        Player one = new Player("one", 2, () -> new Turn(1));
        //and
        //turn has no turns
        one.nextTurn();
        one.nextTurn();

        //expect
        assertThrows(IllegalStateException.class
                , one::getCurrentTurn);

    }
}