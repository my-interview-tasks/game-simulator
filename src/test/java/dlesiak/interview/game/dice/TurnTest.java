package dlesiak.interview.game.dice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class TurnTest {

    @ParameterizedTest(name = "dice throw = {index}")
    @ValueSource(ints = {0, -1, -5})
    void throwErrorIfNumberOfThrowsIsLessThanOrEqualZero(int throwsQuantity) {
        assertThrows(IllegalArgumentException.class, () -> new Turn(throwsQuantity));
    }

    @Test
    void firstThrow() {
        Turn turn = new Turn(10);
        turn.finishThrow();
        assertTrue(turn.firstThrow());
    }

    @Test
    void notFirstThrow() {
        Turn turn = new Turn(10);
        turn.finishThrow();
        turn.finishThrow();
        assertFalse(turn.firstThrow());
    }

    @Test
    void hasAnotherThrow() {
        Turn turn = new Turn(3);
        turn.finishThrow();
        turn.finishThrow();
        assertTrue(turn.hasAnotherThrow());
    }

    @Test
    void hasNoAnotherThrow() {
        Turn turn = new Turn(3);
        turn.finishThrow();
        turn.finishThrow();
        turn.finishThrow();
        assertFalse(turn.hasAnotherThrow());
    }

    @Test
    void throwErrorIfNoAnotherThrow() {
        Turn turn = new Turn(2);
        turn.finishThrow();
        turn.finishThrow();
        assertThrows(IllegalStateException.class
                , turn::finishThrow);
    }
}