package dlesiak.interview.game.dice;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RankingTableTest {

    @Test
    void noDrawWhenFirstPlayersHasDifferentPoints() {
        //given
        //players
        List<Player> players = new ArrayList<>();
        Player one = player("one");
        one.addPenaltyPoints(Points.of(334));
        players.add(one);

        Player three = player("three");
        three.addPenaltyPoints(Points.of(11));
        players.add(three);

        Player two = player("two");
        two.addPenaltyPoints(Points.of(34));
        players.add(two);

        Player four = player("four");
        four.addPenaltyPoints(Points.of(3));
        players.add(four);

        Player five = player("five");
        five.addPenaltyPoints(Points.of(11));
        players.add(five);

        //when
        RankingTable rankingTable = new RankingTable(players);

        //then
        //it is not draw
        assertFalse(rankingTable.isDraw());

        //winner is player four
        assertEquals(four, rankingTable.getWinner());

        //and
        //players are in order
        assertEquals(four.getPoints(), rankingTable.getRanking().get(0).getPoints());
        assertEquals(three.getPoints(), rankingTable.getRanking().get(1).getPoints());
        assertEquals(five.getPoints(), rankingTable.getRanking().get(2).getPoints());
        assertEquals(two.getPoints(), rankingTable.getRanking().get(3).getPoints());
        assertEquals(one.getPoints(), rankingTable.getRanking().get(4).getPoints());

    }

    @Test
    void drawWhenFirstPlayersHasEqualPoints() {
        //given
        //players
        List<Player> players = new ArrayList<>();
        Player one = player("one");
        one.addPenaltyPoints(Points.of(334));
        players.add(one);

        Player three = player("three");
        three.addPenaltyPoints(Points.of(11));
        players.add(three);

        Player two = player("two");
        two.addPenaltyPoints(Points.of(34));
        players.add(two);

        Player four = player("four");
        four.addPenaltyPoints(Points.of(11));
        players.add(four);

        //when
        RankingTable rankingTable = new RankingTable(players);

        //then
        //it is draw
        assertTrue(rankingTable.isDraw());

        //then
        //players with draw
        assertTrue(rankingTable.getWinners().containsAll(Arrays.asList(four, three)));

        //and
        //players are in order
        assertEquals(four.getPoints(), rankingTable.getRanking().get(0).getPoints());
        assertEquals(three.getPoints(), rankingTable.getRanking().get(1).getPoints());
        assertEquals(two.getPoints(), rankingTable.getRanking().get(2).getPoints());
        assertEquals(one.getPoints(), rankingTable.getRanking().get(3).getPoints());
    }

    private Player player(String one) {
        return new Player(one, 2, () -> new Turn(1));
    }


}