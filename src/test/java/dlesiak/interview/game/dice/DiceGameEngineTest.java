package dlesiak.interview.game.dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DiceGameEngineTest {

    private static final Points MAX_PENALTY_POINTS = Points.of(10);
    private final static Predicate<Integer> DEFAULT_WIN_RULE = diceThrowResult -> diceThrowResult == 7 || diceThrowResult == 11;
    private final static Predicate<Integer> DEFAULT_WIN_RULE_2 = diceThrowResult -> diceThrowResult == 5;
    private final static Predicate<Integer> DEFAULT_LOSE_RULE = diceThrowResult -> diceThrowResult == 2 || diceThrowResult == 12;
    private final static int SIX_SIDE_DICE = 6;

    private final Player playerOne = new Player("one", 5, () -> new Turn(5));
    private final Player playerTwo = new Player("two", 5, () -> new Turn(5));
    private final DiceThrower diceThrower = mock(DiceThrower.class);

    private DiceGameEngine diceGameEngine;

    @BeforeEach
    void init() {
        throwsSixSideDice();

        diceGameEngine = new DiceGameEngine(
                new Player[]{playerOne, playerTwo},
                diceThrower,
                new PenaltyPointsCalculator(MAX_PENALTY_POINTS),
                DEFAULT_WIN_RULE,
                DEFAULT_WIN_RULE_2,
                DEFAULT_LOSE_RULE
        );
    }

    @Test
    void throwErrorIfPlayerIsNull() {
        Player[] players = {new Player("name", 10, () -> new Turn(10)), null};
        assertThrows(NullPointerException.class, () -> DiceGameSimulator.defaultGameSimulator(players));
    }

    @Test
    void shouldThrowErrorIfMoreThanOnePlayerHasSameName() {
        Player[] players = {
                new Player("name", 10, () -> new Turn(10)),
                new Player("name", 10, () -> new Turn(10)),
        };
        assertThrows(IllegalArgumentException.class, () -> DiceGameSimulator.defaultGameSimulator(players));
    }

    @Test
    void throwErrorIfNoPlayers() {
        Player[] players = {};
        assertThrows(IllegalArgumentException.class, () -> DiceGameSimulator.defaultGameSimulator(players));
    }

    @Test
    void firstPlayerStartsGame() {
        //when
        diceGameEngine.nextStep();

        //expect
        //playerOne starts game
        assertEquals(playerOne, diceGameEngine.currentPlayer());
    }

    private void throwsSixSideDice() {
        when(diceThrower.diceTotalSides()).thenReturn(SIX_SIDE_DICE);
    }
}