## Dice game simulator

### How to run
mvn clean test install

run GameSimulator class


### Description

Write program that simulate dice game. 

* Two players plays game with names: "pierwszy", "drugi"
* Each player has 5 turns
* Each turn has 10 throws
* Player "pierwszy" starts game, after his turn, player "drugi" starts his turn and so on
* Each player throws with two dices
* Dice can have values {1, 2, 3, 4, 5, 6}
* Each player gets penalty points according to rules. 
* Penalty points at start game for each player equals to 0
* Player with lowest penalty points win the game
* If player throws 7 or 11 in first throw of his turn, he win his turn and next player turn starts
* If player throws 2 or 12 in his first throw of his turn, he loses and next player turn starts. Also, loser gets penalty points that equals to maximum penalty points (as much as he would get in the most pessimistic course of his turn)
* If player throws 5, he win turn and next player turn starts
* If player throw other number than mentioned above, he gets additional penalty points equals to throw result divided by throw number in current turn
* Information of result of game is print to standard output.


